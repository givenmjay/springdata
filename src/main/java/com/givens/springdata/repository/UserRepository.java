/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.givens.springdata.repository;

import com.givens.springdata.model.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author given
 */
public interface UserRepository extends MongoRepository<User, ObjectId>{
    public User findByUsername(String username);    
}
