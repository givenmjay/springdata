/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.givens.springdata.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author given
 */

@Document
public class User {
    
    @Id
    private ObjectId id;
    
    private String firstname;
    private String lastname;
    
    private String username;
    private String password;
    
    
    @DBRef
    private Role role;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }
  
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }   

    @Override
    public String toString() {
        return "{\"id\":\"" + id + "\",\"firstname\":\"" + firstname + "\", \"lastname\":\"" + lastname + "\", \"username\":\"" + username + "\"}";
    }
    
    
}
