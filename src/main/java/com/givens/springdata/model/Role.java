/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.givens.springdata.model;


import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author given
 */

@Document
public class Role {
    
    @Id
    private ObjectId id;
    private String rolename;
    
    private Integer role;

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }
    
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

   

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }    
    
}
